/*
*
* Hier werden API anfragen vom Frontend abgefangen und bearbeitet.
* Dazu werden vom Frontend Get- oder Post-Requests mit Parametern gesendet.
* Diese Parameter findet man hier im @RequestBody in jeder einzelnen Funktion.
* Die Parameter werden vom @RequestBody ausgelesen und and die entsprechende Repository-Abfrage übergeben.
*
*/

package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.controller;

import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.*;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.export.CourseExporter;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.export.CourseTypeExporter;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.export.SchoolTypeExporter;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.export.SupportWorkedExporter;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.repository.*;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.request.*;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.response.AuthenticationResponse;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
public class Controller {

    @Autowired private JwtUtil jwtUtil;

    @Autowired private AuthenticationManager authenticationManager;

    @Autowired private PasswordEncoder passwordEncoder;

    @Autowired private UserRepository              userRepository;
    @Autowired private CourseRepository            courseRepository;
    @Autowired private CourseTypeRepository        courseTypeRepository;
    @Autowired private SchoolTypeRepository        schoolTypeRepository;
    @Autowired private UserSupportRepository       userSupportRepository;
    @Autowired private UserSupportWorkedRepository userSupportWorkedRepository;

//////////// LOGIN / REGISTER ////////////
    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@RequestBody RegisterRequest registerRequest) {
        List<User> users = new ArrayList<>();
        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        User user = new User(registerRequest.getUsername(), registerRequest.getFirstname(), registerRequest.getLastname(), registerRequest.getEmail(), passwordEncoder.encode(registerRequest.getPassword()),date, "", 1);
        users.add(user);

        userRepository.saveAll(users);

        return ResponseEntity.ok(user);

    }

    @PostMapping("/authenticate")
    public ResponseEntity<?> authUser(@RequestBody AuthRequest authRequest) throws Exception {

        try {
            authenticationManager.authenticate( new UsernamePasswordAuthenticationToken(authRequest.getUserName(), authRequest.getPassword()) );
        } catch (Exception ex) {
            throw new Exception("inavalid username/password");
        }

        final String jwt = jwtUtil.generateToken(authRequest.getUserName());
        final long expiration = jwtUtil.extractExpiration(jwt).getTime();
        User user = userRepository.findByUserName(authRequest.getUserName());
        int userid = user.getId();
        int status = user.getStatus();

        return ResponseEntity.ok(new AuthenticationResponse(jwt, expiration, userid, status));

    }

    @GetMapping("/getUsers")
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    @PostMapping("/editUser")
    public ResponseEntity<?>  editUser(@RequestBody RegisterRequest registerRequest) {
        userRepository.updateUser(registerRequest.getUid(), registerRequest.getUsername(), registerRequest.getFirstname(), registerRequest.getLastname(), registerRequest.getEmail());

        if (!registerRequest.getPassword().equals("")){
            userRepository.updatePassword(registerRequest.getUid(), passwordEncoder.encode(registerRequest.getPassword()));
        }

        return ResponseEntity.ok(new AuthenticationResponse(1));
    }

    @PostMapping("/deleteUser")
    public ResponseEntity<?>  deleteUser(@RequestBody RegisterRequest registerRequest) {
        userRepository.deleteUser(registerRequest.getUid(), registerRequest.getStatus());

        return ResponseEntity.ok(new AuthenticationResponse(1));
    }

    @PostMapping("/isExpired")
    public ResponseEntity<?> isExpired(@RequestBody AuthRequest authRequest){

        long currentTime = System.currentTimeMillis();

        if (currentTime >= authRequest.getExpiresIn())
            return ResponseEntity.ok(new AuthenticationResponse(0));
        else
            return ResponseEntity.ok(new AuthenticationResponse(1));
    }

//////////// COURSE ////////////
    @GetMapping("/getCourses")
    public List<Course> getCourses() {

        return courseRepository.findAll();

    }

    @PostMapping("/addCourse")
    public ResponseEntity<?> addCourse(@RequestBody CourseRequest r) {

        List<Course> courses = new ArrayList<>();
        Course course = new Course();

        course.setCnumber(r.getCnumber());
        course.setCname(r.getCname());
        course.setCourseType(courseTypeRepository.findAllByCtid(r.getCctid()).get(0));
        course.setCdate(r.getCdate());
        course.setCtime(r.getCtime());
        course.setCtname(r.getCtname());
        course.setCtmail(r.getCtmail());
        course.setCtphone(r.getCtphone());
        course.setSchoolType(schoolTypeRepository.findAllByStid(r.getCstid()).get(0));
        course.setCclasslevel(r.getCclasslevel());
        course.setCamountpresent(r.getCamountpresent());
        course.setCamountregistrations(r.getCamountregistrations());
        course.setCfee(r.getCfee());
        course.setCstatus(1);

        courses.add(course);
        courseRepository.saveAll(courses);

        return ResponseEntity.ok(1);

    }

    @PostMapping("/updateCourse")
    public int updateCourse(@RequestBody CourseRequest r) {

        courseRepository.updateCourse(r.getCid(), r.getCnumber(), r.getCname(), r.getCctid(), r.getCdate(), r.getCtime(), r.getCtname(), r.getCtmail(), r.getCtphone(), r.getCstid(), r.getCclasslevel(), r.getCamountpresent(), r.getCamountregistrations(), r.getCfee());

        return 1;

    }

    @PostMapping("/deleteCourse")
    public int deleteCourse(@RequestBody CourseRequest request) {

        //courseRepository.updateStatusById(request.getId());

        return 1;

    }

    @GetMapping(path = "/exportCourseExcel/{ids}")
    public ResponseEntity<?> exportCourseExcel(@PathVariable("ids") String[] ids) throws IOException {

        File file = new File(getClass().getResource(".").getFile() + "/test.xlsx");
        file.createNewFile();
        List<Course> courses;

        ArrayList<Integer> servs = new ArrayList<>();
        for (String id : ids) {
            servs.add(Integer.parseInt(id));
        }

        courses = courseRepository.findAllById(servs);

        CourseExporter excelExporter = new CourseExporter(courses);

        return excelExporter.export();

    }

//////////// COURSETYPES ////////////
    @GetMapping("/getCourseTypes")
    public List<CourseType> getCourseTypes() { return courseTypeRepository.findAll(); }

    @PostMapping("/addCourseType")
    public ResponseEntity<?> addCourseType(@RequestBody CourseTypeRequest r) {

        List<CourseType> courseTypes = new ArrayList<>();
        CourseType courseType = new CourseType();

        courseType.setCtname(r.getCtname());
        courseType.setCttype(r.getCttype());
        courseType.setCtsubject(r.getCtsubject());
        courseType.setCtparticipation(r.getCtparticipation());
        courseType.setCtsupport(r.getCtsupport());
        courseType.setCtcare(r.getCtcare());
        courseType.setCtstatus(1);

        courseTypes.add(courseType);
        courseTypeRepository.saveAll(courseTypes);

        return ResponseEntity.ok(1);

    }

    @PostMapping("/updateCourseType")
    public int updateCourseType(@RequestBody CourseTypeRequest r) {
        System.out.println(r);
        courseTypeRepository.updateCourseType(r.getCtid(), r.getCtname(), r.getCttype(), r.getCtsubject(), r.getCtparticipation(), r.getCtsupport(), r.getCtcare());

        return 1;
    }

    @GetMapping(path = "/exportCourseType/{ids}")
    public ResponseEntity<?> exportCourseType(@PathVariable("ids") String[] ids) throws IOException {

        File file = new File(getClass().getResource(".").getFile() + "/Kursdefinition.xlsx");
        file.createNewFile();

        ArrayList<Integer> servs = new ArrayList<>();
        for (String id : ids) {
            servs.add(Integer.parseInt(id)+1);
        }

        return new CourseTypeExporter((Integer.parseInt(ids[0]) != -1) ? courseTypeRepository.findAllById(servs) : courseTypeRepository.findAll()).export();
    }

//////////// SCHOOLTYPES ////////////
    @GetMapping("/getSchoolTypes")
    public List<SchoolType> getSchoolTypes() { return schoolTypeRepository.findAll(); }

    @PostMapping("/addSchoolType")
    public ResponseEntity<?> addSchoolType(@RequestBody SchoolTypeRequest r) {

        List<SchoolType> schoolTypes = new ArrayList<>();
        SchoolType schoolType = new SchoolType();

        schoolType.setStsnumber(r.getStsnumber());
        schoolType.setStsname(r.getStsname());
        schoolType.setStstype(r.getStstype());
        schoolType.setStsadress(r.getStsadress());
        schoolType.setStszip(r.getStszip());
        schoolType.setStsplace(r.getStsplace());
        schoolType.setStsdistrict(r.getStsdistrict());
        schoolType.setStstatus(1);

        schoolTypes.add(schoolType);
        schoolTypeRepository.saveAll(schoolTypes);

        return ResponseEntity.ok(1);

    }

    @PostMapping("/updateSchoolType")
    public int updateSchoolType(@RequestBody SchoolTypeRequest r) {
        System.out.println(r);
        schoolTypeRepository.updateSchoolType(r.getStid(), r.getStsnumber(), r.getStsname(), r.getStstype(), r.getStsadress(), r.getStszip(), r.getStsplace(), r.getStsdistrict());

        return 1;
    }

    @GetMapping(path = "/exportSchoolType/{ids}")
    public ResponseEntity<?> exportSchoolType(@PathVariable("ids") String[] ids) throws IOException {

        File file = new File(getClass().getResource(".").getFile() + "/Schuldefinition.xlsx");
        file.createNewFile();

        ArrayList<Integer> servs = new ArrayList<>();
        for (String id : ids) {
            servs.add(Integer.parseInt(id)+1);
        }

        return new SchoolTypeExporter((Integer.parseInt(ids[0]) != -1) ? schoolTypeRepository.findAllById(servs) : schoolTypeRepository.findAll()).export();
    }

//////////// USERSUPPORT ////////////
    @GetMapping("/getUserSupports")
    public List<Support> getUserSupports() {
        return userSupportRepository.findAll();
    }

    @PostMapping("/addUserSupport")
    public ResponseEntity<?> insertUserSupport(@RequestBody UserSupportRequest request) {
        List<Support> supports = new ArrayList<>();

        Support support = new Support();
        support.setSNameFirst(request.getSNameFirst());
        support.setSNameLast(request.getSNameLast());
        support.setDatecreation( new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
        support.setSstatus(1);

        supports.add(support);

        userSupportRepository.saveAll(supports);

        return ResponseEntity.ok(1);

    }

    @PostMapping("/updateUserSupport")
    public int updateUserSupport(@RequestBody UserSupportRequest r) {

        userSupportRepository.updateUserSupport(r.getSid(), r.getSNameFirst(), r.getSNameLast());

        return 1;

    }

    @PostMapping("/deleteUserSupport")
    public int deleteUserSupport(@RequestBody UserSupportRequest request) {

        userSupportRepository.updateStatusById(request.getSid());

        return 1;

    }

    @GetMapping(path = "/exportUserSupportExcel/{ids}")
    public ResponseEntity<?> exportExcel(@PathVariable("ids") String[] ids) throws IOException {

        File file = new File(getClass().getResource(".").getFile() + "/Studentische Betreuung.xlsx");
        file.createNewFile();
        List<Support> supports;

        ArrayList<Integer> servs = new ArrayList<>();
        for (String id : ids) {
            servs.add(Integer.parseInt(id));
        }

        supports = userSupportRepository.findAllById(servs);

        //TeacherExcelExporter excelExporter = new TeacherExcelExporter(supports);

        return null;//excelExporter.export();

    }

    @GetMapping("/getSupportWorked")
    public List<SupportWorked> getSupportWorked() {
        return userSupportWorkedRepository.findAll();
    }

    @PostMapping("/addSupportWorked")
    public ResponseEntity<?> addSupportWorked(@RequestBody UserSupportRequest r) {

        List<SupportWorked> supportWorkeds = new ArrayList<>();
        SupportWorked supportWorked = new SupportWorked();

        supportWorked.setSwsid(r.getSwsid());
        supportWorked.setSwamount(r.getSwamount());
        supportWorked.setSwdate(r.getSwdate());
        supportWorked.setStatus(1);

        supportWorkeds.add(supportWorked);
        List<SupportWorked> response = userSupportWorkedRepository.saveAll(supportWorkeds);

        return ResponseEntity.ok(response.get(0).getSwid());

    }

    @PostMapping("/delSupportWorked")
    public int delSupportWorked(@RequestBody UserSupportRequest r) {

        userSupportWorkedRepository.updateStatusById(r.getSwid());

        return 1;

    }

    @GetMapping(path = "/exportSupportWorked/{ids}/{year}")
    public ResponseEntity<?> exportSupportWorked(@PathVariable("ids") String[] ids, @PathVariable("year") String year) throws IOException {

        File file = new File(getClass().getResource(".").getFile() + "/Betreuung.xlsx");
        file.createNewFile();

        ArrayList<Integer> servs = new ArrayList<>();
        for (String id : ids) {
            servs.add(Integer.parseInt(id)+1);
        }

        return new SupportWorkedExporter((Integer.parseInt(ids[0]) != -1) ? userSupportRepository.findAllById(servs) : userSupportRepository.findAll(), year).export();
    }

}
