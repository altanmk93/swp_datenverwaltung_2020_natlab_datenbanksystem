package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "calendars")
public class Calendar {

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "date_id")
    private String id;

    @NonNull @Column(name = "weekday")
    private String weekday;

    @NonNull @Column(name = "day")
    private String day;

    @NonNull @Column(name = "month")
    private String month;

    @NonNull @Column(name = "year")
    private String year;

    @NonNull @Column(name = "note_field")
    private String noteField;

}
