package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity;

import lombok.*;

import javax.persistence.*;

@Data
@RequiredArgsConstructor @NoArgsConstructor
@Getter @Setter @Entity
@Table(name = "`course`")
public class Course {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cid")
    private int cid;

    @NonNull @Column(name = "cnumber")
    private String cnumber;

    @NonNull @Column(name = "cname")
    private String cname;

    @NonNull @Column(name = "cdate")
    private String cdate;

    @NonNull @Column(name = "ctime")
    private String ctime;

    @NonNull @Column(name = "ctname")
    private String ctname;

    @NonNull @Column(name = "ctmail")
    private String ctmail;

    @NonNull @Column(name = "ctphone")
    private String ctphone;

    @NonNull @Column(name = "cclasslevel")
    private String cclasslevel;

    @NonNull @Column(name = "camountpresent")
    private int camountpresent;

    @NonNull @Column(name = "camountregistrations")
    private int camountregistrations;

    @NonNull @Column(name = "cfee")
    private float cfee;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @NonNull @JoinColumn(name = "cctid")
    private CourseType courseType;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @NonNull @JoinColumn(name = "cstid")
    private SchoolType schoolType;

    @NonNull @Column(name = "cstatus")
    private int cstatus;

}
