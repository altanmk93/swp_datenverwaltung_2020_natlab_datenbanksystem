package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@RequiredArgsConstructor @NoArgsConstructor
@Getter @Setter @Entity
@Table(name = "`coursetype`")
public class CourseType {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ctid_")
    private int ctid;

    @NonNull @Column(name = "ctname")
    private String ctname;

    @NonNull @Column(name = "cttype")
    private String cttype;

    @NonNull @Column(name = "ctsubject")
    private String ctsubject;

    @NonNull @Column(name = "ctparticipation")
    private String ctparticipation;

    @NonNull @Column(name = "ctsupport")
    private String ctsupport;

    @NonNull @Column(name = "ctcare")
    private String ctcare;

    @NonNull @Column(name = "ctstatus")
    private int ctstatus;

}
