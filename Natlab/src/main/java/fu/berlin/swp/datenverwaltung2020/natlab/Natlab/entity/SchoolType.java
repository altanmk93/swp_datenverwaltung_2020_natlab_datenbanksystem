package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@RequiredArgsConstructor @NoArgsConstructor
@Getter @Setter @Entity
@Table(name = "`schooltype`")
public class SchoolType {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "stid")
    private int stid;

    @NonNull @Column(name = "stsnumber")
    private String stsnumber;

    @NonNull @Column(name = "stsname")
    private String stsname;

    @NonNull @Column(name = "ststype")
    private String ststype;

    @NonNull @Column(name = "stsadress")
    private String stsadress;

    @NonNull @Column(name = "stszip")
    private String stszip;

    @NonNull @Column(name = "stsplace")
    private String stsplace;

    @NonNull @Column(name = "stsdistrict")
    private String stsdistrict;

    @NonNull @Column(name = "ststatus")
    private int ststatus;

}
