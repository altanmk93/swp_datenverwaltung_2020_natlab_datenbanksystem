package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity;

import com.sun.istack.Nullable;
import lombok.*;

import javax.persistence.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Getter @Setter
@AllArgsConstructor @NoArgsConstructor
@Entity @Table(name = "`support`")
public class Support {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "sid")
    private int sid;

    @NonNull @Column(name = "snamefirst")
    private String sNameFirst;

    @NonNull @Column(name = "snamelast")
    private String sNameLast;

    @NonNull @Column(name = "sdatecreation")
    private String datecreation;

    @NonNull @Column(name = "sdatedelete")
    private String datedelete;

    @NonNull @Column(name = "sstatus")
    private Integer sstatus;

    // Foregin Key
    @OneToMany(mappedBy = "swsid", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<SupportWorked> supportWorked;

}

