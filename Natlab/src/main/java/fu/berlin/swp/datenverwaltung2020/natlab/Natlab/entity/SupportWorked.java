package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity;

import lombok.*;

import javax.persistence.*;

@Data @Getter @Setter
@AllArgsConstructor @NoArgsConstructor
@Entity @Table(name = "`supportworked`")
public class SupportWorked {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "swid")
    private int swid;

    @NonNull @Column(name = "swsid")
    private int swsid;

    @NonNull @Column(name = "swamount")
    private float swamount;

    @NonNull @Column(name = "swdate")
    private String swdate;

    @NonNull @Column(name = "swstatus")
    private int status;

}

