package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity @Setter @Getter
@Table(name = "teachers")
public class Teacher {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "teacher_id")
    private int id;

    @Column(name = "school_branch")
    private String school_branch;

    @Column(name = "grad")
    private String grad;

    @Column(name = "firstname")
    private String firstname;

    @Column(name = "lastname")
    private String lastname;

    @Column(name = "gender")
    private String gender;

    @Column(name = "personal_number")
    private String personal_number;

    @Column(name = "email")
    private String email;

    @Column(name = "school_name")
    private String school_name;

    @Column(name = "subject")
    private String subject;

    @Column(name = "school_number")
    private int school_number;

    @Column(name = "telephone")
    private String telephone;

    @Column(name = "street")
    private String street;

    @Column(name = "zip")
    private int zip;

    @Column(name = "city")
    private String city;

    @Column(name = "status")
    private int status;

/*
    @ManyToMany
    @JoinTable(
            name = "user_teacher",
            joinColumns = @JoinColumn(name = "teacher_id"),
            inverseJoinColumns = @JoinColumn(name = "uid"))
    private List<User> useristeacher;

    @ManyToMany
    @JoinTable(
            name = "course_like",
            joinColumns = @JoinColumn(name = "teacher_id"),
            inverseJoinColumns = @JoinColumn(name = "uid"))
    List<User> likedCourses;

    @ManyToMany(mappedBy = "teachers")
    private List<Course> courses;

    @ManyToMany(mappedBy = "teachers")
    private List<Student> students;
*/

}
