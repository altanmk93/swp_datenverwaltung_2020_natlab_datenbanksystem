package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Data
@RequiredArgsConstructor
@NoArgsConstructor
@Getter @Setter @Entity
@Table(name = "`user`")
public class User {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "uid")
    private int id;

    @NonNull @Column(name = "uname")
    private String userName;

    @NonNull @Column(name = "unamefirst")
    private String firstName;

    @NonNull @Column(name = "unamelast")
    private String lastName;

    @NonNull @Column(name = "uemail")
    private String email;

    @NonNull @Column(name = "upassword")
    private String password;

    @NonNull @Column(name = "udatecreation")
    private String datecreation;

    @NonNull @Column(name = "udatedelete")
    private String datedelete;

    @NonNull @Column(name = "ustatus")
    private int status;

/*
    @ManyToMany(mappedBy = "useristeacher")
    private List<Teacher> userteacher;

 */
/*
    @ManyToMany(mappedBy = "likedCourses")
    private Set<Teacher> likes;
*/
/*
    @ManyToMany(mappedBy = "user")
    private List<Course> courses;

    @ManyToMany(mappedBy = "user")
    private List<Lectureship> lectureships;

    @ManyToMany(mappedBy = "user")
    private List<Student> students;

    @ManyToMany(mappedBy = "user")
    private List<Calendar> calendars;
*/

}

