package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.export;

import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.CourseType;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class CourseTypeExporter {

    private final XSSFWorkbook workbook;
    private final XSSFSheet sheet;

    private final List<CourseType> courseTypes;
    private int rowIndex = 1;

    public CourseTypeExporter(List<CourseType> courseTypes) {
        this.courseTypes = courseTypes;
        workbook = new XSSFWorkbook();
        sheet = workbook.createSheet("Kursdefinition");
    }

    private void writeHeaderRow(){

        XSSFFont font = workbook.createFont();
        font.setBold(true);

        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setFont(font);

        Row row = sheet.createRow(0);

        Cell cell = row.createCell(0);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Kursname");

        cell = row.createCell(1);
        cell.setCellValue("Kursart");

        cell = row.createCell(2);
        cell.setCellValue("Fach");

        cell = row.createCell(3);
        cell.setCellValue("Beteiligung");

        cell = row.createCell(4);
        cell.setCellValue("Förderung");

        cell = row.createCell(5);
        cell.setCellValue("Betreuung");

    }

    private void writeDateRows(){

        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setAlignment(HorizontalAlignment.LEFT);

        for (CourseType courseType: courseTypes) {

            Row row = sheet.createRow(rowIndex);
            rowIndex++;

            Cell cell = row.createCell(0);
            cell.setCellValue(courseType.getCtname());

            cell = row.createCell(1);
            cell.setCellValue(courseType.getCttype());

            cell = row.createCell(2);
            cell.setCellValue(courseType.getCtsubject());

            cell = row.createCell(3);
            cell.setCellValue(courseType.getCtparticipation());

            cell = row.createCell(4);
            cell.setCellValue(courseType.getCtsupport());

            cell = row.createCell(5);
            cell.setCellValue(courseType.getCtcare());

        }
        sheet.autoSizeColumn(0);
        sheet.autoSizeColumn(1);
        sheet.autoSizeColumn(2);
        sheet.autoSizeColumn(3);
        sheet.autoSizeColumn(4);
        sheet.autoSizeColumn(5);

    }

    public ResponseEntity<InputStreamResource> export() throws IOException {
        writeHeaderRow();
        writeDateRows();

        File file = new File(getClass().getResource(".").getFile() + "/Kursdefinition.xlsx");
        file.createNewFile();
        FileOutputStream outputStream = new FileOutputStream(file, false);
        workbook.write(outputStream);
        workbook.close();
        outputStream.close();

        String headerKey = "Content-Disposition";
        String headerValue = "attachement; filename=Kursdefinition.xlsx";

        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));

        return ResponseEntity.ok().header(headerKey, headerValue)
                .contentLength(file.length())
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(resource);
    }

}
