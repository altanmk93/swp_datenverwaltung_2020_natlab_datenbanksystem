package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.export;

import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.SchoolType;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class SchoolTypeExporter {

    private final XSSFWorkbook workbook;
    private final XSSFSheet sheet;

    private final List<SchoolType> schoolTypes;
    private int rowIndex = 1;

    public SchoolTypeExporter(List<SchoolType> schoolTypes) {
        this.schoolTypes = schoolTypes;
        workbook = new XSSFWorkbook();
        sheet = workbook.createSheet("Schuldefinition");
    }

    private void writeHeaderRow(){

        XSSFFont font = workbook.createFont();
        font.setBold(true);

        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setFont(font);

        Row row = sheet.createRow(0);

        Cell cell = row.createCell(0);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Schulnummer");

        cell = row.createCell(1);
        cell.setCellValue("Schulname");

        cell = row.createCell(2);
        cell.setCellValue("Schulart");

        cell = row.createCell(3);
        cell.setCellValue("Strasse");

        cell = row.createCell(4);
        cell.setCellValue("PLZ");

        cell = row.createCell(5);
        cell.setCellValue("Ort");

        cell = row.createCell(6);
        cell.setCellValue("Bezirk");

    }

    private void writeDateRows(){

        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setAlignment(HorizontalAlignment.LEFT);

        for (SchoolType schoolType: schoolTypes) {

            Row row = sheet.createRow(rowIndex);
            rowIndex++;

            Cell cell = row.createCell(0);
            cell.setCellValue(schoolType.getStsnumber());

            cell = row.createCell(1);
            cell.setCellValue(schoolType.getStsname());

            cell = row.createCell(2);
            cell.setCellValue(schoolType.getStstype());

            cell = row.createCell(3);
            cell.setCellValue(schoolType.getStsadress());

            cell = row.createCell(4);
            cell.setCellValue(schoolType.getStszip());

            cell = row.createCell(5);
            cell.setCellValue(schoolType.getStsplace());

            cell = row.createCell(6);
            cell.setCellValue(schoolType.getStsdistrict());

        }
        sheet.autoSizeColumn(0);
        sheet.autoSizeColumn(1);
        sheet.autoSizeColumn(2);
        sheet.autoSizeColumn(3);
        sheet.autoSizeColumn(4);
        sheet.autoSizeColumn(5);
        sheet.autoSizeColumn(6);

    }

    public ResponseEntity<InputStreamResource> export() throws IOException {
        writeHeaderRow();
        writeDateRows();

        File file = new File(getClass().getResource(".").getFile() + "/Schuldefinition.xlsx");
        file.createNewFile();
        FileOutputStream outputStream = new FileOutputStream(file, false);
        workbook.write(outputStream);
        workbook.close();
        outputStream.close();

        String headerKey = "Content-Disposition";
        String headerValue = "attachement; filename=Schuldefinition.xlsx";

        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));

        return ResponseEntity.ok().header(headerKey, headerValue)
                .contentLength(file.length())
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(resource);
    }

}
