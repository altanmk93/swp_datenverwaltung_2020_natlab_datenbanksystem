package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.export;

import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.Support;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.SupportWorked;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SupportWorkedExporter {

    private final XSSFWorkbook workbook;
    private XSSFSheet sheet;

    private final List<Support> supports;
    private int rowIndex = 1;
    private final String year;
    private String month;

    public SupportWorkedExporter(List<Support> supports, String year) {
        this.year = year;
        this.supports = supports;
        workbook = new XSSFWorkbook();

        for (int month = 1; month < 13; month++){
            this.month = (month < 10) ? "0"+month : ""+month;
            this.sheet = workbook.createSheet(month+"."+year.substring(2,4));
            this.rowIndex = 1;
            writeHeaderRow();
            writeDateRows();
        }
    }

    private void writeHeaderRow(){

        XSSFFont font = workbook.createFont();
        font.setBold(true);

        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setFont(font);

        Row row = sheet.createRow(0);

        Cell cell = row.createCell(0);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Name");
        cell = row.createCell(1);
        cell.setCellValue("Vorname");
        cell = row.createCell(2);
        cell.setCellValue("1");
        cell = row.createCell(3);
        cell.setCellValue("2");
        cell = row.createCell(4);
        cell.setCellValue("3");
        cell = row.createCell(5);
        cell.setCellValue("4");
        cell = row.createCell(6);
        cell.setCellValue("5");
        cell = row.createCell(7);
        cell.setCellValue("6");
        cell = row.createCell(8);
        cell.setCellValue("7");
        cell = row.createCell(9);
        cell.setCellValue("8");
        cell = row.createCell(10);
        cell.setCellValue("9");
        cell = row.createCell(11);
        cell.setCellValue("10");
        cell = row.createCell(12);
        cell.setCellValue("11");
        cell = row.createCell(13);
        cell.setCellValue("12");
        cell = row.createCell(14);
        cell.setCellValue("13");
        cell = row.createCell(15);
        cell.setCellValue("14");
        cell = row.createCell(16);
        cell.setCellValue("15");
        cell = row.createCell(17);
        cell.setCellValue("16");
        cell = row.createCell(18);
        cell.setCellValue("17");
        cell = row.createCell(19);
        cell.setCellValue("18");
        cell = row.createCell(20);
        cell.setCellValue("19");
        cell = row.createCell(21);
        cell.setCellValue("20");
        cell = row.createCell(22);
        cell.setCellValue("21");
        cell = row.createCell(23);
        cell.setCellValue("22");
        cell = row.createCell(24);
        cell.setCellValue("23");
        cell = row.createCell(25);
        cell.setCellValue("24");
        cell = row.createCell(26);
        cell.setCellValue("25");
        cell = row.createCell(27);
        cell.setCellValue("26");
        cell = row.createCell(28);
        cell.setCellValue("27");
        cell = row.createCell(29);
        cell.setCellValue("28");
        cell = row.createCell(30);
        cell.setCellValue("29");
        cell = row.createCell(31);
        cell.setCellValue("30");
        cell = row.createCell(32);
        cell.setCellValue("31");
        cell = row.createCell(33);
        cell.setCellValue("Stunden");
        cell = row.createCell(34);
        cell.setCellValue("Entgelt");

    }

    private void writeDateRows(){

        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setAlignment(HorizontalAlignment.LEFT);

        for (Support support: supports) {

            Row row = this.sheet.createRow(rowIndex);
            rowIndex++;

            float hours = 0.0f;

            Cell cell = row.createCell(0);
            cell.setCellValue(support.getSNameLast());
            sheet.autoSizeColumn(0);
            cell = row.createCell(1);
            cell.setCellValue(support.getSNameFirst());
            sheet.autoSizeColumn(1);

            for (int i = 1; i < 32; i++){
                String day = (i < 10) ? "0"+i : ""+i;
                List<SupportWorked> d = Arrays.stream(support.getSupportWorked().toArray(new SupportWorked[0])).filter(x -> x.getStatus() == 1 && x.getSwdate().contains(year+"-"+month+"-"+day)).collect(Collectors.toList());
                if (d.size() > 0) hours = hours+d.get(0).getSwamount();

                cell = row.createCell(i+1);
                cell.setCellValue((d.size() > 0) ?  String.valueOf(d.get(0).getSwamount()) : "");
                sheet.autoSizeColumn(i+1);
            }

            cell = row.createCell(33);
            cell.setCellValue(String.valueOf(hours));
            sheet.autoSizeColumn(33);
            cell = row.createCell(34);
            cell.setCellValue(hours*10+" €");
            sheet.autoSizeColumn(34);
        }

    }

    public ResponseEntity<InputStreamResource> export() throws IOException {

        File file = new File(getClass().getResource(".").getFile() + "/Betreuung.xlsx");
        file.createNewFile();
        FileOutputStream outputStream = new FileOutputStream(file, false);
        workbook.write(outputStream);
        workbook.close();
        outputStream.close();

        String headerKey = "Content-Disposition";
        String headerValue = "attachement; filename=Betreuung.xlsx";

        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));

        return ResponseEntity.ok().header(headerKey, headerValue)
                .contentLength(file.length())
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(resource);
    }

}
