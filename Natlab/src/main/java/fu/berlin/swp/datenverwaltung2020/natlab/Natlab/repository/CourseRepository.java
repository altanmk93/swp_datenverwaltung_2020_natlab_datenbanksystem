package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.repository;

import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.Course;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

@Repository
public interface CourseRepository extends JpaRepository<Course, Integer> {

    List<Course> findAllByCid(int cid);

    @Transactional @Modifying
    @Query("FROM Course WHERE cstatus = 1")
    List<Course> findAll();

    @Transactional @Modifying
    @Query("UPDATE Course SET cnumber = :cnumber, cname = :cname, cctid = :cctid, cdate = :cdate, ctime = :ctime, ctname = :ctname, ctmail = :ctmail, ctphone = :ctphone, cstid = :cstid, cclasslevel = :cclasslevel, camountpresent = :camountpresent, camountregistrations = :camountregistrations, cfee = :cfee WHERE cid = :cid")
    void updateCourse(@Param("cid")int cid, @Param("cnumber")String cnumber, @Param("cname")String cname, @Param("cctid")int cctid, @Param("cdate")String cdate, @Param("ctime")String ctime, @Param("ctname")String ctname, @Param("ctmail")String ctmail, @Param("ctphone")String ctphone, @Param("cstid")int cstid, @Param("cclasslevel")String cclasslevel, @Param("camountpresent")int camountpresent, @Param("camountregistrations")int camountregistrations, @Param("cfee")float cfee);

}
