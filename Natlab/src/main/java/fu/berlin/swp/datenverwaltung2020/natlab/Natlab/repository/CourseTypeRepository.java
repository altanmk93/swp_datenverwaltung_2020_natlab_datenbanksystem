package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.repository;

import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.CourseType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface CourseTypeRepository extends JpaRepository<CourseType, Integer> {

    @Transactional @Modifying
    @Query("FROM CourseType WHERE ctid_ = :ctid ORDER BY ctid")
    List<CourseType> findAllByCtid(int ctid);

    @Transactional @Modifying
    @Query("FROM CourseType WHERE ctstatus = 1 ORDER BY ctid")
    List<CourseType> findAll();

    @Transactional @Modifying
    @Query("UPDATE CourseType SET ctname = :ctname, cttype = :cttype, ctsubject = :ctsubject, ctparticipation = :ctparticipation, ctsupport = :ctsupport, ctcare = :ctcare WHERE ctid_ = :ctid")
    void updateCourseType(@Param("ctid")int ctid, @Param("ctname")String ctname, @Param("cttype")String cttype, @Param("ctsubject")String ctsubject, @Param("ctparticipation")String ctparticipation, @Param("ctsupport")String ctsupport, @Param("ctcare")String ctcare);
}
