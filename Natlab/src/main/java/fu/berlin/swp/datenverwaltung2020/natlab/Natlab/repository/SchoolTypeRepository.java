package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.repository;

import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.SchoolType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface SchoolTypeRepository extends JpaRepository<SchoolType, Integer> {

    @Transactional @Modifying
    @Query("FROM SchoolType WHERE stid = :stid ORDER BY stid")
    List<SchoolType> findAllByStid(int stid);

    @Transactional @Modifying
    @Query("FROM SchoolType WHERE ststatus = 1 ORDER BY stid")
    List<SchoolType> findAll();

    @Transactional @Modifying
    @Query("UPDATE SchoolType SET stsnumber = :stsnumber, stsname = :stsname, ststype = :ststype, stsadress = :stsadress, stszip = :stszip, stsplace = :stsplace, stsdistrict = :stsdistrict WHERE stid = :stid")
    void updateSchoolType(@Param("stid")int stid, @Param("stsnumber")String stsnumber, @Param("stsname")String stsname, @Param("ststype")String ststype, @Param("stsadress")String stsadress, @Param("stszip")String stszip, @Param("stsplace")String stsplace, @Param("stsdistrict")String stsdistrict);

}
