package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.repository;

import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.Support;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

public interface UserRepository extends JpaRepository<User, Integer> {

    @Transactional @Modifying
    @Query("FROM User ORDER BY uid ASC")
    List<User> findAll();

    @Transactional @Modifying
    @Query("UPDATE User SET uname = :uname, unamefirst = :unamefirst, unamelast = :unamelast, uemail =:uemail WHERE uid = :uid")
    void updateUser(@Param("uid")int uid, @Param("uname")String uname, @Param("unamefirst")String unamefirst, @Param("unamelast")String unamelast, @Param("uemail")String uemail);

    @Transactional @Modifying
    @Query("UPDATE User SET upassword = :upassword WHERE uid = :uid")
    void updatePassword(@Param("uid") int uid, @Param("upassword") String upassword);

    @Transactional @Modifying
    @Query("UPDATE User SET ustatus = :ustatus WHERE uid = :uid")
    void deleteUser(@Param("uid") int uid, @Param("ustatus") int ustatus);

    User findByUserName(String username);

}
