package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.repository;

import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.Support;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface UserSupportRepository extends JpaRepository<Support, Integer> {

    @Transactional @Modifying
    @Query("FROM Support WHERE sstatus = 1")
    List<Support> findAll();

    @Transactional @Modifying
    @Query("UPDATE Support SET snamefirst = :snamefirst, snamelast = :snamelast WHERE sid = :sid")
    void updateUserSupport(@Param("sid") int sid, @Param("snamefirst") String snamefirst,  @Param("snamelast") String snamelast );

    @Transactional @Modifying
    @Query("UPDATE Support s SET s.sstatus = 0 WHERE s.sid = :sid")
    void updateStatusById(@Param("sid") Integer sid);

}