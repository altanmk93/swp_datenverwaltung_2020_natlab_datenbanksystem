package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.repository;

import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.SupportWorked;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface UserSupportWorkedRepository extends JpaRepository<SupportWorked, Integer> {

    @Transactional @Modifying
    @Query("FROM SupportWorked WHERE swstatus = 1 ORDER BY swid")
    List<SupportWorked> findAll();

    @Transactional @Modifying
    @Query("UPDATE SupportWorked SET swstatus = 0 WHERE swid = :swid")
    void updateStatusById(@Param("swid") Integer swid);
}