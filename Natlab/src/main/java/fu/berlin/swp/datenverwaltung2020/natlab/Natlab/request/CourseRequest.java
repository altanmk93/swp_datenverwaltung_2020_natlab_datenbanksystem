package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data @Getter
@AllArgsConstructor @NoArgsConstructor
public class CourseRequest {

    private int cid;
    private String cnumber;
    private String cname;
    private Integer cctid;
    private String cdate;
    private String ctime;
    private String ctname;
    private String ctmail;
    private String ctphone;
    private int cstid;
    private String cclasslevel;
    private int camountpresent;
    private int camountregistrations;
    private float cfee;
    private int cstatus;

}