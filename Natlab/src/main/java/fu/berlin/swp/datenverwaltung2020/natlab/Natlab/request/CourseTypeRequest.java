package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data @Getter
@AllArgsConstructor @NoArgsConstructor
public class CourseTypeRequest {

    private int    ctid;
    private String ctname;
    private String cttype;
    private String ctsubject;
    private String ctparticipation;
    private String ctsupport;
    private String ctcare;
    private int    ctstatus;

}