package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.request;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter @Setter
public class RegisterRequest {

    private int uid;
    private String username;
    private String firstname;
    private String lastname;
    private String email;
    private String password;
    private int status;

}