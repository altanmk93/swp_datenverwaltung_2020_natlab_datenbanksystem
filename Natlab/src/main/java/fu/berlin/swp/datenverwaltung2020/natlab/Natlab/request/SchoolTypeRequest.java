package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data @Getter
@AllArgsConstructor @NoArgsConstructor
public class SchoolTypeRequest {

    private int    stid;
    private String stsnumber;
    private String stsname;
    private String ststype;
    private String stsadress;
    private String stszip;
    private String stsplace;
    private String stsdistrict;
    private int    ststatus;

}