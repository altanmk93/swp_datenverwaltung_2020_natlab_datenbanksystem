package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data @Getter
@AllArgsConstructor @NoArgsConstructor
public class UserRequest {

    private int uid;
    private String username;
    private String unamefirst;
    private String unamelast;
    private String uemail;
    private String upassword;
    private String udatecreation;
    private String udatedelete;
    private int    ustatus;

}