package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data @Getter
@AllArgsConstructor @NoArgsConstructor
public class UserSupportRequest {

    private int sid;
    private String sNameFirst;
    private String sNameLast;

    // SupportWorked
    private int    swid;
    private int    swsid;
    private float  swamount;
    private String swdate;


}