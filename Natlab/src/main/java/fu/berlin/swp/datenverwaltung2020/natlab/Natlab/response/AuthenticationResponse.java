package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.response;

import java.util.Date;

public class AuthenticationResponse {

    private final String idToken;
    private final long expiresIn;
    private int userid;
    private int status;

    public AuthenticationResponse(String idToken, long expiresIn, int userid, int status) {
        this.idToken = idToken;
        this.expiresIn = expiresIn;
        this.userid = userid;
        this.status = status;
    }

    public AuthenticationResponse(int value) {
        this.idToken = null;
        this.expiresIn = 0;
        this.userid = 0;

        switch (value){
            case 0:
                this.status = 0;
                break;
            case 1:
                this.status = 1;
                break;

        }
    }

    public int getUser() {
        return userid;
    }

    public void setUser(int userid) {
        this.userid = userid;
    }

    public String getIdToken() {
        return idToken;
    }

    public long getExpiresIn() {
        return expiresIn;
    }

    public int getStatus() {
        return status;
    }
}
