package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.service;

import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.SchoolType;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.repository.CourseRepository;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.repository.SchoolTypeRepository;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class DataImportService {

    @Autowired private CourseRepository courseRepository;
    @Autowired private SchoolTypeRepository schoolTypeRepository;

    @PostConstruct @Transactional
    public void importSchoolTypes() {
        List<SchoolType> data = schoolTypeRepository.findAll();
        if (data.size() == 0)
            try {
                FileInputStream fis = new FileInputStream(new File("src/main/resources/Schuldefinition.xlsx"));
                XSSFWorkbook wb = new XSSFWorkbook(fis);

                //creating a Sheet object to retrieve the object
                int numberOfSheets = wb.getNumberOfSheets();
                List<SchoolType> schoolDefinitionen = new ArrayList<>();

                // for loop for sheets
                for (int i = 0; i < numberOfSheets; i++) {
                    XSSFSheet sheet = wb.getSheetAt(i);

                    for (int j = 1; j < sheet.getPhysicalNumberOfRows() && sheet.getRow(j).getCell(0) != null; j++) {
                        Row row = sheet.getRow(j);

                        SchoolType schoolType = new SchoolType();
                        schoolType.setStsnumber(row.getCell(0).getStringCellValue());
                        schoolType.setStsname(row.getCell(1).getStringCellValue());
                        schoolType.setStstype(row.getCell(2).getStringCellValue());
                        schoolType.setStsadress(row.getCell(3).getStringCellValue());
                        schoolType.setStszip(row.getCell(4).getStringCellValue());
                        schoolType.setStsplace(row.getCell(5).getStringCellValue());
                        schoolType.setStsdistrict(row.getCell(6).getStringCellValue());
                        schoolType.setStstatus(1);

                        schoolDefinitionen.add(schoolType);
                    }


                }

                schoolTypeRepository.saveAll(schoolDefinitionen);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

}