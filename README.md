# Controller
Hier werden API anfragen vom Frontend abgefangen und bearbeitet.  
Dazu werden vom Frontend Get- oder Post-Requests mit Parametern gesendet.  
Diese Parameter findet man hier im [@RequestBody](https://git.imp.fu-berlin.de/altanmk93/swp_datenverwaltung_2020_natlab_datenbanksystem/-/blob/master/README.md#request) in jeder einzelnen Funktion.  
Die Parameter werden vom [@RequestBody](https://git.imp.fu-berlin.de/altanmk93/swp_datenverwaltung_2020_natlab_datenbanksystem/-/blob/master/README.md#request) ausgelesen und and die entsprechende [Repository-Abfrage](https://git.imp.fu-berlin.de/altanmk93/swp_datenverwaltung_2020_natlab_datenbanksystem/-/blob/master/README.md#repository) übergeben. 

# Entity
Hier werden die Datenbanktabellen definiert.   
Jede Entitiy ist eine eigene Tabelle.   
In der Entitiy werden alle Columns definiert.   

# Repository
Hier werden die SQL-Abfragen definiert.  
Nachdem im Controller die Parameter übergeben wurden,   
werden diese mit vordefinierten SQL-Abfragen an die Datenbank übergeben.  

# Request
Hier werden die Parameter für das @RequestBody definiert.  
Nur die hier definierten Variablen können vom Frontend übersendet werden.  

# DataImportService
Dieser Service dient dazu Daten aus einer Excel-Tabelle in die Datenbank hinzuzufügen.

# JwtUtil
Hier werden der Benutzername und das Passwort überprüft und bei übereinstimmung ein json-web-token (JWT) generiert.  
Dieser ist begrenzt gültig. Nur mit gültigem JWT können Backend-Request abgerufen werden.


